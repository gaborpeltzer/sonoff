//
// Sonoff custom firmware
// @GaborPeltzer
//

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

const char* ssidAP = "Sonoff";
const char* passphraseAP = "s0n0ff!!";
int relayPin = 12;
int ledPin = 13;
int buttonPin = 0;
int httpServerPort = 80;
int wifiTimeOut = 120; //~2min

ESP8266WebServer httpServer(httpServerPort);

void setup(void) {
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, LOW);

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);

  Serial.begin(115200);
  EEPROM.begin(512);
  delay(10);
  Serial.println("setup");

  String ssid;
  for (int i = 0; i < 32; ++i) {
    ssid += char(EEPROM.read(i));
  }
  Serial.println(ssid);

  String passphrase = "";
  for (int i = 32; i < 96; ++i) {
    passphrase += char(EEPROM.read(i));
  }

  if (ssid.length() > 1 && passphrase.length() > 1) {
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid.c_str(), passphrase.c_str());

    if (!checkWifiConnect()) {
      setupAP();
    }
  } else {
    setupAP();
  }

  createHttpServer();
}

void loop(void) {
  httpServer.handleClient();
}

bool checkWifiConnect(void) {
  Serial.println("checkWifiConnect");
  int counter = 0;

  while (WiFi.status() != WL_CONNECTED) {
    if (counter == wifiTimeOut) {
      Serial.println("wifiTimeOut");
      WiFi.disconnect();
      return false;
    }

    delay(1000);
    counter++;
  }

  return true;
}

void createHttpServer(void) {
  Serial.println("createHttpServer");
  httpServer.on("/", []() {
    statusResponse(String(digitalRead(relayPin)));
  });

  httpServer.on("/on", []() {
    digitalWrite(ledPin, LOW);//led on!
    digitalWrite(relayPin, HIGH);
    statusResponse(String(digitalRead(relayPin)));
  });

  httpServer.on("/off", []() {
    digitalWrite(ledPin, HIGH);//led off!
    digitalWrite(relayPin, LOW);
    statusResponse(String(digitalRead(relayPin)));
  });

  //  post or get ssid and pass params to http://192.168.4.1/set
  httpServer.on("/set", []() {
    String ssid = httpServer.arg("ssid");
    String passphrase = httpServer.arg("pass");
    String content;

    if (ssid.length() > 0 && passphrase.length() > 0) {
      for (int i = 0; i < 96; ++i) {
        EEPROM.write(i, 0);
      }

      for (int i = 0; i < ssid.length(); ++i) {
        EEPROM.write(i, ssid[i]);
      }

      for (int i = 0; i < passphrase.length(); ++i) {
        EEPROM.write(32 + i, passphrase[i]);
      }

      EEPROM.commit();
      statusResponse("ok");

      delay(500);
      WiFi.disconnect();
      setup();// ESP.restart() or ESP.reset() is not work!
    } else {
      statusResponse("error");
    }
  });

  httpServer.on("/clear", []() {
    for (int i = 0; i < 96; ++i) {
      EEPROM.write(i, 0);
    }

    EEPROM.commit();
    statusResponse("ok");

    delay(2000);
    WiFi.disconnect();
    setup();// ESP.restart() or ESP.reset() is not work!
  });

  httpServer.begin();
}

void setupAP(void) {
  Serial.println("setupAP");
  WiFi.softAP(ssidAP, passphraseAP, 6);
}

void statusResponse(String status) {
    String response = "{\"status\":\"";
    response.concat(status);
    response.concat("\"}");
    httpServer.send(200, "application/json", response);
}
